<?php
class Admin_Form_Element_VendorSelect extends Zend_Form_Element_Select {
    public function init() {
        $vendorTb = new Application_Model_DbTable_Vendor();
        $this->addMultiOption(0, 'Please select vendor...');      
        foreach ($vendorTb->fetchAll() as $vendorTb) {
            $this->addMultiOption($vendorTb['VendorId'], $vendorTb['name']);
        }
    }
}
