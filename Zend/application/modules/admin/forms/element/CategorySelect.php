<?php
class Admin_Form_Element_CategorySelect extends Zend_Form_Element_Select {
    public function init() {
        $CategoryTb = new Application_Model_DbTable_Category();
        $this->addMultiOption(0, 'New category');      
        foreach ($CategoryTb->fetchAll() as $CategoryTb) {
            $this->addMultiOption($CategoryTb['CategoryId'], $CategoryTb['name']);
        }
    }
}
