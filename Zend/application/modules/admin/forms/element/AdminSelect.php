<?php
class Admin_Form_Element_AdminSelect extends Zend_Form_Element_Select {
    public function init() {
        $CategoryTb = new Application_Model_DbTable_Admin();
        $this->addMultiOption(0, 'Please select Admin');     
        foreach ($CategoryTb->fetchAll() as $CategoryTb) {
            $this->addMultiOption($CategoryTb['AdminId'], $CategoryTb['email']);
        }
    }
}
