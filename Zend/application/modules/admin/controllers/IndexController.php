<?php

class Admin_IndexController extends Zend_Controller_Action {

    protected $vendor;

    public function init() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('admin/auth/index');
        }
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_helper->redirector('index');
    }

    public function allvendorAction() {
        // action body
        $file = new Application_Model_DbTable_Vendor();

        $this->view->file = $file->fetchAll();
    }

    public function addvendorAction() {
        // action body

        $form = new Admin_Form_Vendor();
        $form->submit->setlabel('add');
        $this->view->form = $form;
        if ($this->getRequest()->ispost()) {
            $formData = $this->getRequest()->getpost();
            if ($form->isvalid($formData)) {
                $file = new Application_Model_DbTable_Vendor();
                $name = $form->getvalue('name');
                $file->addfile($name);
                $this->_helper->redirector('allvendor');
            } else {
                $this->populate($formData);
            }
        }
    }

    public function editvendorAction() {
        $form = new Admin_Form_Vendor();
        $this->view->form = $form;

        if ($this->getRequest()->ispost()) {
            $formData = $this->getRequest()->getpost();
            if ($form->isvalid($formData)) {
                $id = $form->getvalue('id');
                $name = $form->getvalue('name');
                $file = new Application_Model_DbTable_Vendor();
                $file->updatefile($id, $name);
                $this->_helper->redirector('allvendor');
            } else {
                $form->populate($formData);
            }
        } else {
            $id = $this->getRequest()->getparam('id');
            if ($id > 0) {
                $file = new Application_Model_DbTable_Vendor();
                $files = $file->fetchRow('VendorId=' . $id);
                $form->populate($files->toArray());
            }
        }
    }

    public function deletevendorAction() {
        //action body

        if ($this->getRequest()->ispost()) {
            $del = $this->getRequest()->getpost('del');
            if ($del == 'Yes') {
                $id = $this->getRequest()->getpost('id');
                $file = new Application_Model_DbTable_Vendor();
                $id = $this->getrequest()->getparam('id');
                $file->deletefile($id);
            }
            $this->_helper->redirector('allvendor');
        } else {
            $id = $this->getRequest()->getparam('id');
            $file = new Application_Model_DbTable_Vendor();
            $this->view->file = $file->getfile($id);
        }
    }

    //Category

    public function allcategoryAction() {
        // action body
        $file = new Application_Model_DbTable_Category();

        $this->view->file = $file->fetchAll();
    }

    public function addcategoryAction() {
        // action body

        $form = new Admin_Form_Category();
        $form->submit->setlabel('add');
        $this->view->form = $form;
        if ($this->getRequest()->ispost()) {
            $formData = $this->getRequest()->getpost();
            if ($form->isvalid($formData)) {
                $file = new Application_Model_DbTable_Category();
                $category = $form->getvalue('category');
                $parent = $form->getvalue('categoryId');
                if (!$parent == 0) {
                    $file->addfile($category, $parent);
                } else {
                    $file->addfile($category, NULL);
                }
                $this->_helper->redirector('allcategory');
            } else {
                $this->populate($formData);
            }
        }
    }

    public function deletecategoryAction() {
        //action body

        if ($this->getRequest()->ispost()) {
            $del = $this->getRequest()->getpost('del');
            if ($del == 'Yes') {
                $id = $this->getRequest()->getpost('id');
                $file = new Application_Model_DbTable_Category();
                $id = $this->getrequest()->getparam('id');
                $file->deletefile($id);
            }
            $this->_helper->redirector('allcategory');
        } else {
            $id = $this->getRequest()->getparam('id');
            $file = new Application_Model_DbTable_Category();
            $this->view->file = $file->getfile($id);
        }
    }

    //Product

    public function allproductAction() {
        // action body
        $file = new Application_Model_DbTable_Product();

        $this->view->file = $file->fetchAll();
    }

    public function addproductAction() {
        // action body

        $form = new Admin_Form_Product();
        $form->submit->setlabel('add');
        $this->view->form = $form;
        if ($this->getRequest()->ispost()) {
            $formData = $this->getRequest()->getpost();
            if ($form->isvalid($formData)) {
                $file = new Application_Model_DbTable_Product();
                $name = $form->getvalue('name');
                $price = $form->getvalue('price');
                $quantity = $form->getvalue('quantity');
                $description = $form->getvalue('description');
                $picture = $form->getvalue('picture');
                $cat = $form->getvalue('CategoryId');
                $vendor = $form->getvalue('VendorId');
                $admin = $form->getvalue('AdminId');
                $file->addfile($name, $price, $quantity, $description, $picture, $cat, $vendor, $admin);
                $this->_helper->redirector('allproduct');
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination("/upload");
                try {
                    // upload received file(s)
                    $upload->receive();
                } catch (Zend_File_Transfer_Exception $e) {
                    $e->getMessage();
                }
            } else {
                $this->populate($formData);
            }
        }
    }

    public function deleteproductAction() {
        //action body

        if ($this->getRequest()->ispost()) {
            $del = $this->getRequest()->getpost('del');
            if ($del == 'Yes') {
                $id = $this->getRequest()->getpost('id');
                $file = new Application_Model_DbTable_Product();
                $id = $this->getrequest()->getparam('id');
                $file->deletefile($id);
            }
            $this->_helper->redirector('allproduct');
        } else {
            $id = $this->getRequest()->getparam('id');
            $file = new Application_Model_DbTable_Product();
            $this->view->file = $file->getfile($id);
        }
    }

}

