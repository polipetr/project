<?php

class Deafault_FacebookController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $facebook = new Facebook_Facebook(array(
            'appId' => '1402161656708633',
            'secret' => 'c1d8b31867c4f870f5241b9c0a9161fe'
        ));

        $user = $facebook->getUser();
        $this->view->user = $user;       

        if ($user) {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me');
                $this->view->userprofile=$user_profile;            
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }

        if ($user) {         
            $logoutUrl = $facebook->getLogoutUrl();
           // session_destroy();
            $this->view->logout=$logoutUrl;
        } else {
            $statusUrl = $facebook->getLoginStatusUrl();
            $this->view->status=$statusUrl;
            $loginUrl = $facebook->getLoginUrl(array(
                'scope'=>'user_birthday,user_hometown,user_location'
            ));
             
            $this->view->login=$loginUrl;
        }
        
       
    }

}

