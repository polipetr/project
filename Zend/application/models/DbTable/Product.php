<?php
class Application_Model_DbTable_Product extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'product';
    protected $_primary='ProductId';
    
     public function getfile($id) {
        $id = (int) $id;

        $row = $this->fetchRow('ProductId = ' . $id);

        if (!$row) {
            throw new Exception("Error could not find row $id");
        }

        return $row->toArray();
    }

    public function addfile($category, $price, $quantity, $description, $picture, $cat, $vendor, $admin) {
        $data = array(
            'name' => $category,
            'price' => $price,
            'quantity' => $quantity,
            'description' => $description,
            'picture' => $picture,
            'VendorId' => $vendor,
            'CategoryId' => $cat,
            'AdminId' => $admin
        );
        $this->insert($data);
    }

    public function deletefile($id) {

        $this->delete('ProductId =' . (int) $id);
    }

}

