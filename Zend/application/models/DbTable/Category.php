<?php

class Application_Model_DbTable_Category extends Zend_Db_Table_Abstract {

    protected $_name = 'category';
    protected $_primary = 'CategoryId';

    public function getfile($id) {
        $id = (int) $id;

        $row = $this->fetchRow('CategoryId = ' . $id);

        if (!$row) {
            throw new Exception("Error could not find row $id");
        }

        return $row->toArray();
    }

    public function addfile($name, $cat) {
        $data=array(
         'name'=>$name,
         'parentid'=>$cat, 
       );
       $this->insert($data);
    }


    public function deletefile($id) {

        $this->delete('CategoryId =' . (int) $id);
    }

}
