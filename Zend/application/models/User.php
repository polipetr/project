<?php

class Application_Model_User extends Zend_Db_Table_Abstract {
     
    
     protected $_name = 'users';
    
    
     public function addfile($faceid,$name,$email,$birthday,$password,$salt,$aname,$location) {
        $data = array(
            'facebookid' => $faceid,
            'name'=>$name,
            'email'=>$email,
            'birthday'=>$birthday,
            'password' => sha1($password.$salt),
            'salt' => $salt,
            'avatar'=>$aname,
            'location'=>$location
            
        );

        $this->insert($data);
    }
    
    public function getLocation($location){
        $row=$this->fetchRow($location);
        return $row->toArray();
    }
      
}
