SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `productcat` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `productcat`;


-- -----------------------------------------------------
-- Table `mydb`.`Admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Admin` (
  `AdminId` INT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  PRIMARY KEY (`AdminId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Vendor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Vendor` (
  `VendorId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`VendorId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Category` (
  `CategoryId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `parentid` INT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`CategoryId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Product` (
  `name` VARCHAR(255) NOT NULL,
  `price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 000,
  `quantity` INT UNSIGNED NOT NULL,
  `description` TEXT NULL,
  `picture` VARCHAR(255) NULL,
  `ProductId` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `VendorId` INT NULL,
  `CategoryId` INT NULL,
  `AdminId` INT NULL,
  PRIMARY KEY (`ProductId`),
  INDEX `AdminId_idx` (`AdminId` ASC),
  INDEX `VendorId_idx` (`VendorId` ASC),
  INDEX `CategoryId_idx` (`CategoryId` ASC),
  CONSTRAINT `AdminId`
    FOREIGN KEY (`AdminId`)
    REFERENCES `Admin` (`AdminId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `VendorId`
    FOREIGN KEY (`VendorId`)
    REFERENCES `Vendor` (`VendorId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `CategoryId`
    FOREIGN KEY (`CategoryId`)
    REFERENCES `Category` (`CategoryId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `Category_log` (
  `CategoryId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `parentid` INT UNSIGNED NULL DEFAULT NULL,
  `ts` TIMESTAMP,
  INDEX (CategoryId))
ENGINE = InnoDB;

DELIMITER $$
CREATE TRIGGER ins
AFTER INSERT ON Category for each ROW
begin 
INSERT INTO Category_log(CategoryId,name,parentid,ts)
Values (new.CategoryId, new.name, new.parentid, NOW());
END$$

DELIMITER ;

CREATE TABLE IF NOT EXISTS `Vendor_log` (
  `VendorId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
    `ts` TIMESTAMP,
  INDEX(`VendorId`))
ENGINE = InnoDB;

DELIMITER $$
CREATE TRIGGER v
AFTER INSERT ON Vendor for each ROW
begin 
INSERT INTO Vendor_log(VendorId,name,ts)
Values (new.VendorId, new.name, NOW());
END$$

DELIMITER ;

