<?php
require_once('includes/header.php');
require_once('config.php');
$data = array();
$index = array();
$query = $con->query(
        "SELECT p.name AS ProductName, p.price,p.description, p.quantity, p.picture, c.CategoryId, c.parentId, c.name AS CategoryName, v.name AS VendorName
FROM category AS c
LEFT JOIN Product AS p ON c.CategoryId = p.CategoryId
LEFT JOIN Vendor AS v ON v.VendorId = p.VendorId
LEFT JOIN admin AS a ON p.AdminId = a.AdminId");
while ($row = mysqli_fetch_assoc($query)) {
    $id = $row["CategoryId"];
    $parent_id = $row["parentId"] === NULL ? "NULL" : $row["parentId"];
    $data[$id] = $row;
    $index[$parent_id][] = $id;
    
}

function display_child_nodes($parent_id, $level) {
    global $data, $index;
    $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
    if (isset($index[$parent_id])) {
        foreach ($index[$parent_id] as $id) {           
             echo '<ul>';                   
             echo'<ul>';
             if($level==0){
             echo'<li>';
             echo  $data[$id]["VendorName"];
             echo'</li>';
             echo'<li>';
             echo  $data[$id]["ProductName"];
             echo'</li>';
             echo'<li>';
             echo  $data[$id]["price"];
             echo'</li>';
             echo'<li>';
             echo  $data[$id]["description"];
             echo'</li>';
             echo'<li>';
             echo  $data[$id]["quantity"];
             echo'</li>';
             echo'<li>';
             echo  $data[$id]["picture"];
             echo'</li>';
             }
             echo'<li>';
             echo str_repeat("-", $level) . $data[$id]["CategoryName"] . "\n";
             echo'</li>';
             echo'</ul>';
             echo'</li>';
             echo '</ul>';           
            // echo $data[$id]["CategoryName"].$data[$id]["price"].$data[$id]["VendorName"];
            display_child_nodes($id, $level + 1);
        }
        
        
        echo "<br/>";
    }
}

display_child_nodes(NULL, 0);