<?php 
	$pageTitle = 'Category';
	require_once('includes/header.php'); 
	require_once('config.php');  
  
	if(isset($_SESSION['is_logged']) && $_SESSION['is_logged'] == true) { 
?>
 <form class="form-inline" role="form" method="POST">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Name</label>
    <input name="catname" type="text" class="form-control" id="exampleInputEmail2" placeholder="Enter name">
  </div>
  <label class="checkbox-inline">Category
  	<select name="parentId" class="form-control">
        <option>NULL</option>; 	
        <?php while($row = mysqli_fetch_array($result0)):?>
                  <option><?php echo $row['ProductId'];?> </option>;          
        <?php endwhile ?>    
   </select>
  </label>
  <button name="submit" type="submit" class="btn btn-default">Add</button>
  </form>

<?php
   
    if(isset($_POST['submit'])){
	$catname= mysql_real_escape_string($_POST['catname']);
    $parentId=($_POST['parentId']);
   $n=new db();
    $n->connect();
    $n->insertCategory($catname,$parentId);
	}
  ?>  
			
<?php
	}
	else {
		header('Location: login.php');
		exit;
	}